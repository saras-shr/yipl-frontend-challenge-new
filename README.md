# YIPL Frontend Challenge

Completed by Saras Sthapit Shrestha

## Description

This is an attempt at YIPL Frontend Challenge posted in https://github.com/younginnovations/internship-challenges/blob/master/readme.md#younginnovations-internship-challenge. HTML and CSS were used to transform the given image into a responsive webpage.

## Visual

![Screenshot of the webpage:] (screenshots/screenshot-01.png)



